class DeviseCustomMailer < Devise::Mailer
  # if self.included_modules.include?(AbstractController::Callbacks)
  #   raise "You've already included AbstractController::Callbacks, remove this line."
  # else
  #   include AbstractController::Callbacks
  # end
  helper :application # gives access to all helpers defined within `application_helper`.
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`
  default template_path: 'devise/mailer' # to make sure that your mailer uses the devise views
  include AbstractController::Callbacks

  before_filter :add_inline_attachment!

  # def confirmation_instructions(record)
  #   super
  # end
  #
  # def reset_password_instructions(record)
  #   super
  # end
  #
  # def unlock_instructions(record)
  #   super
  # end
  #
  # private
  def add_inline_attachment!
    attachments.inline['shipper.png'] = File.read(File.join(Rails.root,'app','assets','images','shipper.png'))
  end
end
