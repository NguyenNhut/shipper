class ApplicationMailer < ActionMailer::Base
  # before_filter :add_inline_attachment!
  default from: Figaro.env.mail_username
  layout 'mailer'
  # attachments['logo_dealmaker.png'] = File.read(Rails.root.join('/assets/logo_dealmaker.png'))

  def add_inline_attachment!
    attachments.inline['shipper.png'] = File.read(File.join(Rails.root,'app','assets','images','shipper.png'))
  end
end
