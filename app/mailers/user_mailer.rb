class UserMailer < ApplicationMailer

  def send_mail_lock(user)
    @user = user
    mail(to: @user.email, subject: 'Tài khoản của bạn đã bị khóa')
  end

  def send_mail_unlock(user)
    @user = user
    mail(to: @user.email, subject: 'Tài khoản của bạn đã được mở khóa')
  end

  def send_mail_payment(user)
    @user = user
    mail(to: @user.email, subject: 'Bạn vừa nạp tiền vào tài khoản')
  end

end
