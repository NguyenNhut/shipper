
function readURL(input) {
     if (input.files && input.files[0]) {
       var reader = new FileReader();

       reader.onload = function (e) {
         $('#img_prev')
           .attr('src', e.target.result)
           .width(80)
           .height(80);
       };

       reader.readAsDataURL(input.files[0]);
     }
   }

$(window).load(function() {
   $('.loader').fadeOut();
   $('.page-loader').delay(150).fadeOut('slow');
});

$(document).ready(function(){
  setTimeout(function(){
    $('.success').remove();
    $('.errors').remove();
    $('.message').remove();
   }, 5000);

   $('#btnSignup').click(function(){
     $('#login-form').hide();
     $('#signup-form').show();
   });
   $('#loginForm').click(function(){
     $('#signup-form').hide();
     $('#login-form').show();
   });
   $('#signupnow').click(function(){
     $('#login-form').hide();
     $('#signup-form').show();
   });
   $('#loginnow').click(function(){
     $('#signup-form').hide();
     $('#login-form').show();
   });

    // var d = new Date();
    // var year = d.getFullYear() - 18;
    // d.setFullYear(year);
    // $('#datepicker').datepicker({ changeYear: true, changeMonth: true, yearRange: '1990:' + year + '', defaultDate: d});

  $('.datepicker').datepicker({
    format: "dd/mm/yyyy"
    // changeMonth: true,
    // changeYear: true,
    // yearRange: "1990:1999",
    // maxDay: new Date(),
    // defaultDate: '1/1/1990'
   });

   $('.datetimepicker').datetimepicker({
     format:'DD/MM/YYYY HH:mm',
     useCurrent: false
   });

  $('#btn_login').click(function(){

    console.log('===============');
    var email = $('#login-email').val();
    var password = $('#login-password').val();

    $.ajax({
      url: "/users/sign_in",
      type: "POST",
      data: {user: {
        email: email,
        password: password}},
      success: function(res){
        console.log(res);
        if(res.success){
          if(res.confirmed) {
            alertify.success(res.message);
            console.log(res);
            if(res.admin){
              window.location.href= "/users";
            }else{
              window.location.href = "/posts";
            }
          }
          else {
            alertify.error(res.message);
          }
        }else{
          alertify.error(res.message);
        }
      }
      });
});


  $('#btnSignUp').click(function(){
    console.log('===============');
    var email = $('#login-email2').val();
    var password = $('#login-password2').val();
    var repassword = $('#login-repassword2').val();
    console.log(password);
    console.log(repassword);

    $.ajax({
      url: "/users",
      type: "POST",
      data: {user: {
        email: email,
        password: password,
        password_comfirmation: repassword
        }},
        success: function(res){
          console.log(res);
          if(res.success){
            alertify.success(res.message);
          }else{
            alertify.error(res.message);
          }
        }
      });
  });

  $('.btn-payment').click(function(){
    console.log("=================");
    var self = $(this);
    var user_id = self.parents('.modal-payment').find('.user_id').val();
    var money = self.parents('.modal-payment').find('.money').val();
    console.log(user_id);
    console.log(money);
    $.ajax({
      url: "/admin_payment",
      type: "POST",
      data: {payment: {
        money: money,
        user_id: user_id
      }},
      success: function(res){
        if(res.success){
          window.location.href= "/users";

        }else{
          $('.alert-danger').remove();
          $('.message-payment').prepend(' \
          <div class="alert alert-danger"> \
          '+res.message+' \
          </div> \
          ');
      }
    }
  });
});

$('.btn_bid').click(function(e){
  e.preventDefault();
  console.log("=================");

  var self = $(this);
  var post_id = self.parents('.modal-bid').find('.post_id').val();
  var bid_price= self.parents('.modal-bid').find('.bid_price').val();
  var bid_comment = self.parents('.modal-bid').find('.bid_comment').val();
  console.log(post_id);
  console.log(bid_price);
  console.log(bid_comment);

  $.ajax({
    url: "/bids",
    type: "POST",
    data: {
      bid: {
      post_id: post_id,
      bid_price: bid_price,
      bid_comment: bid_comment
    }},
    success: function(data){
      if(data.success){
        alertify.success(data.message);
      }else{
        alertify.error(data.message);
        // $('.message-bid').prepend(' \
        // <div class="alert alert-danger"> \
        // '+data.message+' \
        // </div> \
        // ');
    }
  }
});
});


  $('.delete-user').click(function(event){
    var self = $(this);
    $.ajax({
      url: "users/"+self.attr("data-target"),
      type: "delete",
      success: function(res){
        console.log(res);
        if(res.success){
        $('.alert-success').remove();
        self.parents('.box-user').remove();
        $('.message').prepend(' \
        <div class="alert alert-success"> \
        Delete success! \
        </div> \
        ');
      }
    }
  });
});

$('.lock-user').click(function(event){
  var self = $(this);
  console.log("=========");
  $.ajax({
    url: 'users/'+self.attr("data-target")+'/lock_user',
    type: "get",
    success: function(res){
      console.log(res);
      if(res.success){
        alertify.success(res.message);
        if(res.locked) {
          self.html('<i class="fa fa-unlock"></i> Mở khóa');
        }else {
          self.html('<i class="fa fa-lock"></i> Khóa');
        }

      }
  }
});
});

  // setInterval(function(){ check_notication(); }, 3000);
  //
  // function check_notication() {
  //   $.ajax({
  //     url: "/notifications",
  //     type: "GET",
  //     success: function(res){
  //        var noti = res.data.noti;
  //        var noti_user = res.data.noti_user;
  //        var noti_restaurant = res.data.noti_restaurant;
  //        var restaurant_hash = res.data.restaurant_hash;
  //        if (noti != 0) {
  //          $('.notification').append('<span class="number_notification">'+noti+'</span>');
  //          $('.notification_ul').find('li').remove();
  //          if(noti_user != 0){
  //            $('.notification_ul').append('<li> <a href="/dashboard/orders/orders_shipping_user">'+ noti_user +' đơn hàng được chấp nhận</a> </li>');
  //          }
  //          if(noti_restaurant != 0){
  //            for (var i = 0; i < restaurant_hash.length; i++) {
  //              $('.notification_ul').append('<li> <a href="/dashboard/restaurants/'+restaurant_hash[i].id+'/orders/orders_pending"> '+ restaurant_hash[i].name +' có '+restaurant_hash[i].num +' đặt hàng</a> </li>');
  //            }
  //          }
  //        }
  //     }
  //   });
  // };

  $('#seen_notifications').click(function(){
    var self= $(this);
    console.log("==============");
    $.ajax({
      url: "seen_notifications",
      type: "post",
      success: function(res){
        console.log(res);
        $('#view-notification').remove();
      }
    });
  });

  $('.delete-notification').click(function(event){
    var self = $(this);
    console.log("==============");
    $.ajax({
      url: "notifications/"+self.attr("data-target"),
      type: "delete",
      success: function(res){
        self.parents('.notification-item').find('.view-notification').html(res.data.notifications);
        if(res.success){
          console.log(res);
          self.parents('.box-notification').remove();
        }

      }
    });
  });

  $('.delete-post').click(function(event){
    var self = $(this);
    $.ajax({
      url: "posts/"+self.attr("data-target"),
      type: "delete",
      success: function(res){
        console.log(res);
        if(res.success){
          self.parents('.post-item').remove();
          alertify.success(res.message);
          if(res.counter_post == 0) {
            $('.post_empty').prepend(' \
              <div class="col-md-4 alert alert-danger no-post col-sm-offset-4">\
              <div>Bạn không có bài đăng nào</div>\
              <div class="top-20"><a href="<%= new_post_path %>" class="btn btn-vip">Đăng bài</a></div>\
              </div>\
            ');
          }
        }else {
          alertify.error(res.message);

        }
    }
  });
});

// Delete bid
$('.delete-bid').click(function(){
  var self = $(this);
  console.log("==============");
  $.ajax({
    url: '/bids/'+self.attr("data-target"),
    type: 'delete',
    success: function(res){
      if(res.success){
        self.parents('.box-user-bid').remove();
        alertify.success(res.message);
      }else {
        alertify.error(res.message);
      }
    }
  });
});

$('.choose-shipper').click(function(){
  var self = $(this);
  console.log("============");
  $.ajax({
    url: '/shippers',
    type: 'post',
    data: {
      shipper: {
        bid_id: self.attr("data-target")
      }
    },
    success: function(res) {
      if(res.success) {
        console.log("============");
        window.location.href= "/my_post_true";
        alertify.success(res.message);
      }else {
        alertify.error(res.message);
      }
    }

  });
});

$('.delete-shipper').click(function(){
  var self = $(this);
  console.log("==============");
  $.ajax({
    url: '/shippers/'+self.attr("data-target"),
    type: 'delete',
    success: function(res){
      if(res.success){
        self.parents('.box-shipper').remove();
        alertify.success(res.message);
      }else {
        alertify.error(res.message);
      }
    }
  });
});

$('.review').click(function(){
  var self = $(this);
  var score = self.parents(".reviews-content").find('.star-rating').raty('score');
  var comment = self.parents(".reviews-content").find('.comment').val();
  var shipper_id = self.attr("data-target");
  console.log(score);
  console.log(comment);
  console.log(shipper_id);
  $.ajax({
    url: '/reviews',
    type: 'post',
    data: {
      review: {
        score: score,
        comment: comment,
        shipper_id: shipper_id
      }
    },
    success: function(res) {
      if(res.success) {
        var okl = self.parents(".reviews");
        okl.find('.reviews-content').remove();
        okl.prepend(' \
        <div class="col-md-5 btm-10"><b>Đánh giá:</b></div>\
        <div class="reviews col-md-7 btm-10">\
          <div class="star-rated" data-score="'+score+'"></div>\
        </div>\
        <div class="col-md-5 btm-10"><b>Nhận xét:</b></div>\
        <div class="col-md-7 btm-10">'+comment+'</div>');
        $('.star-rated').raty({
          path: "/assets",
          scoreName: "review[score]",
          readOnly: true,
          score: function() {
            return $(this).attr("data-score");
          }
        });
        alertify.success(res.message);
      }else {
        alertify.error(res.message);
      }
    }

  });
});

$('#btnReport').click(function(){
  var self = $(this);
  var report_email = $('#report-email').val();
  var report_comment = $('#report-comment').val();
  console.log("==============");

  $.ajax({
    url: '/reports',
    type: 'post',
    data: {
      report: {
        email: report_email,
        comment: report_comment
      }
    },
    success: function(res){
      if(res.success){
        alertify.success(res.message);
      }else {
        alertify.error(res.message);
      }
    }
  });
});

$('.delete-report').click(function(){
  var self = $(this);
  console.log("==============");
  $.ajax({
    url: '/reports/'+self.attr("data-target"),
    type: 'delete',
    success: function(res){
      if(res.success){
        self.parents('.report-item').remove();
        alertify.success(res.message);
      }else {
        alertify.error(res.message);
      }
    }
  });
});

$('.delete-report').click(function(){
  var self = $(this);
  console.log("==============");
  $.ajax({
    url: '/payments/'+self.attr("data-target"),
    type: 'delete',
    success: function(res){
      if(res.success){
        self.parents('.payment-item').remove();
        alertify.success(res.message);
      }else {
        alertify.error(res.message);
      }
    }
  });
});

// $('.star-rating').raty({
//   path: "/assets",
//   scoreName: "review[score]"
// });
//   $('#star-rated').raty({
//     path: "/assets",
//     scoreName: "review[score]",
//     readOnly: true,
//     score: function() {
//       return $(this).attr("data-score");
//     }
//   });
});
