module ApplicationHelper
  def time_ago_converter(words)
      pattern = {'hour'=>'giờ', 'minute'=>'phút', 'day'=>'ngày',
        'hours'=>'giờs', 'minutes'=>'phút', 'days'=>'ngày', 'about' => '', 'less than a phút trước' => 'vừa xong',
        'phúts trước' => 'phút trước', 'giờs trước' => 'giờ trước', 'ngàys trước' => 'ngày trước', "phúts" => "phút",
        'month' => "tháng", 'ngàys' => 'ngày', 'less than a phút' => 'vừa xong', 'vừa xong trước' => 'vừa xong'
        }
      pattern.each_pair do |k, v|
        words.gsub!(k, v)
      end
      words
    end

  def format_history_day(day)
    if (Time.now - day.to_time)/1.day > 1
      return day.strftime("%d/%m/%Y %I:%M %p")
    else
      day = time_ago_converter time_ago_in_words(day)
      return day
    end
  end

  def method_payment(payment)
    return "Thanh toán trực tiếp" if payment.card_token == "Admin"
    return "Thanh toán bằng thẻ"
  end

end
