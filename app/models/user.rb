class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  has_one :profile, dependent: :destroy
  has_many :notifications, dependent: :delete_all
  has_many :posts, dependent: :delete_all
  has_many :bids, dependent: :delete_all
  has_many :shippers, dependent: :delete_all
  has_many :reviews, through: :shippers
  has_many :reports
  has_many :payments


  def is_admin
    return true if self.role == 1
    return false
  end

  def locked?
    return true if self.lock == true
    return false
  end

  def has_profile
    return true if self.profile != nil
    return false
  end

  def sum_score
    sum = 0
    self.reviews.each do |review|
      sum += review.score
    end
    return sum
  end

  def confirmation_required?
    true
  end

  def self.search(email)
     where("email LIKE ?", "%#{email}%")
    #  where("place_to LIKE ?", "%#{to}%")
   end

  class <<self
    def unlock_user
      @users = User.where(lock: true)
      @users.each do |user|
        if (Time.now - user.date_lock)/1.day >= 30
          user.update(lock: false, date_lock: nil)
          UserMailer.send_mail_unlock(user).deliver_now
        end
      end
    end
  end

end
