class Profile < ApplicationRecord
  belongs_to :user

  validates :name, :presence => {:message => "Không được để trống"}
  validates :date_of_birth, :presence => {:message => "Không được để trống"}
  validates :male, :presence => {:message => "Không được để trống"}
  validates :address, :presence => {:message => "Không được để trống"}
  validates :phone, :presence => {:message => "Không được để trống"},
                    :numericality => {only_integer: true, :message => "Không được chứa ký tự"},
                    :length => { is: 10 || 11, :message => "Phải là 10 hoặc 11 số" }
  validates :identity_card, :presence => {:message => "Không được để trống"},
                            :numericality => {only_integer: true, :message => "Không được chứa ký tự"},
                            :length => { is: 9 || 12, :message => "Phải là 9 hoặc 12 số" }
  validates :money, :presence => {:message => "Không được để trống"},
                    :numericality => {only_integer: true, :message => "Không được chứa ký tự"}

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/assets/user.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
end
