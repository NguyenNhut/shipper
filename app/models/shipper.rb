class Shipper < ApplicationRecord
  belongs_to :user
  has_one :review
  belongs_to :post
end
