class Post < ApplicationRecord
  is_impressionable
  belongs_to :user
  has_many :bids, dependent: :delete_all
  has_one :shipper, dependent: :destroy

  validates :price, :presence => {:message => "Chưa có giá cho bài đăng"}
  validates :title, :presence => {:message => "Không được để trống"}
  validates :place_from, :presence => {:message => "Không được để trống"}
  validates :place_to, :presence => {:message => "Không được để trống"}
  validates :time_receive, :presence => {:message => "Không được để trống"}
  validates :time_send, :presence => {:message => "Không được để trống"}
  validate :check_time_recieve
  validate :check_time_send

  def self.search(from, to)
  # Title is for the above case, the OP incorrectly had 'name'
  if from.empty? == false && to.empty? == false
     where("place_from LIKE ?", "%#{from}%")
     where("place_to LIKE ?", "%#{to}%")
  elsif from.empty?
     where("place_to LIKE ?", "%#{to}%")
   else
     where("place_from LIKE ?", "%#{from}%")
   end
 end

 def check_time_recieve
   if self.time_receive > Time.now
     return true
   else
     errors.add(:time_receive, "Thời gian nhận không hợp lệ")
   end
 end

 def check_time_send
   if self.time_send > self.time_receive
     return true
   else
     errors.add(:time_send, "Thời gian giao phải lớn hơn thời gian nhận")
   end
 end

 class <<self
   def delete_post_deadline
     @posts = Post.all.where(status: false)
     @posts.each do |post|
       if post.time_receive < Time.now
         post.destroy
       end
     end
   end
 end


end
