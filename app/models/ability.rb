class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.is_admin
      can :manage, User
      can :manage, Profile
      can :manage, Configurable
      can :manage, Payment
      can :manage, Report
    else
      can [:change_password, :show], User
      can :manage, Post
      can :manage, Bid
      can :manage, Shipper
      can :manage, Notification
      can :manage, Profile
      can [:create], Report
      can :manage, Review
      can [:new, :create, :destroy], Payment
    end
  end
end
