class NotificationsController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user
  before_action :check_locked
  before_action :authenticate_profile

  def index
    @notification = current_user.notifications
  end
  def destroy
    @notification = Notification.find_by id: params[:id]
    if @notification.destroy
      render :json => {
        :success => true,
        :data => {:notifications => current_user.notifications.count}
      }
    else
      render :json => {:success => false}
    end
  end

  def seen_notifications
    @notifications = current_user.notifications.where(status: false)
    if @notifications.update(status: true)
      render :json => {
        :success => true,
        :data => {
          :notifications => current_user.notifications.where(status: false).count
        }
      }
    end
  end
end
