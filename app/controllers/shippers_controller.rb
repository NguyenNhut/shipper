class ShippersController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user
  before_action :check_locked
  before_action :authenticate_profile

  def index
    @shippers = current_user.shippers
  end

  def create
    @bid = Bid.find_by id: params[:shipper][:bid_id].to_i
    @post= @bid.post
    @user = @bid.user
    params[:shipper][:money_ship] = @bid.bid_price
    params[:shipper][:fee] = params[:shipper][:money_ship] * Configurable.first.percent_fee / 100
    last_money = @user.profile.money - params[:shipper][:fee]
    @user.profile.update(money: last_money)
    @shipper = @post.create_shipper(shipper_params.merge(user_id: @user.id))
    if @shipper.save
      @post.bids.destroy_all
      @post.update(status: true)
      @user.notifications.create(send_id: current_user.id, content: "Bạn vừa được chấp nhận đấu giá ở bài đăng #{@post.title}")
      message = "Chọn shipper thành công"
      render :json => {:success => true, :message => message}
    else
      message = "Chọn shipper thất bại"
      render :json => {:success => false, :message => message}
    end
  end

  def destroy
    @shipper = Shipper.find_by_id params[:id]
    if @shipper.review
      if @shipper.destroy
        render :json => {:success => true, :message => "Xóa thành công"}
      else
        render :json => {:success => false, :message => "Xóa thất bại"}
      end
    else
      if @shipper.user == current_user
        render :json => {:success => false, :message => "Bạn chưa được đánh giá"}
      else
        render :json => {:success => false, :message => "Bạn phải đánh giá shipper"}
      end
    end
  end


  private

  def shipper_params
    params.require(:shipper).permit(:user_id, :money_ship, :fee)
  end
end
