class ReportsController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user
  before_action :check_locked
  before_action :authenticate_profile
  
  def create
    @user = User.find_by email: params[:report][:email]
    if @user
      params[:report][:user_id] = @user.id
      if @user.reports.where(user_report: current_user.id).empty?
        @report = @user.reports.new report_params.merge(user_report: current_user.id)
        if @report.save
          if @user.reports.count >=  3
            @user.update(lock: true, date_lock: Time.now)
            UserMailer.send_mail_lock(@user).deliver_now
          end
          render :json => {:success => true, :message => "Tố cáo thành công"}
        else
          render :json => {:success => false, :message => "Tố cáo thất bại"}
        end

      else
        render :json => {:success => false, :message => "Bạn đã tố cáo người này rồi"}
      end
    else
      render :json => {:success => false, :message => "Người dùng không tồn tại"}
    end
  end

  def destroy
    @report = Report.find_by id: params[:id]
    if @report.destroy
      render :json => {:success => true, :message => "Xóa thành công"}
    else
      render :json => {:success => false, :message => "Xóa thất bại"}
    end
  end

  private

  def report_params
    params.require(:report).permit(:user_report, :comment, :user_id)
  end

end
