class ApplicationController < ActionController::Base
  include CanCan::ControllerAdditions
  protect_from_forgery with: :exception
  # before_action :authenticate_user
  # before_action :authenticate_profile
  after_filter :format_error, :if => :devise_controller?

  def after_sign_in_path_for(resource_or_scope)
    if current_user.profile
        posts_path
    else
        new_profile_path
    end
  end

  # def current_path
  #   if current_user.is_admin?
  #     root_path
  #   elsif current_user.is_lawyer?
  #      lawyer_dashboard_index_path
  #    else
  #      investor_dashboard_index_path
  #    end
  #  end

  def after_sign_out_path_for(resource_or_scope)
   root_path
  end

  # def authenticate_user
  #   if !current_user
  #     flash[:error] = "Vui lòng đăng nhập"
  #     redirect_to root_path
  #   else
  #     if current_user.locked?
  #       sign_out(current_user)
  #       flash[:error] = "Tài khoản đã bị khóa"
  #       redirect_to warning_index_path
  #     else
  #     if !current_user.profile
  #       flash[:error] = "Vui lòng cập nhật thông tin trước"
  #       redirect_to new_profile_path
  #     end
  #   end
  # end
  # end

  def authenticate_user
    if !current_user
      flash[:error] = "Vui lòng đăng nhập"
      redirect_to root_path
    end
  end

  def authenticate_profile
    if !current_user.profile
      flash[:error] = "Vui lòng cập nhật thông tin trước"
      redirect_to new_profile_path
    end
  end

  def format_error
    if resource && resource.errors.empty? == false
      flash[:error] = resource.errors.full_messages.join(" ")
    end
  end

  def check_locked
    if current_user.locked?
      sign_out(current_user)
      flash[:error] = "Tài khoản của bạn đã bị khóa"
      redirect_to warning_index_path
    end
  end

end
