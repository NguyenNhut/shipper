class ReviewsController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user
  before_action :check_locked
  before_action :authenticate_profile

  def index
    @reviews = current_user.reviews.paginate(:page => params[:page], :per_page => 4)
  end

  def create
    @shipper = Shipper.find_by id: params[:review][:shipper_id]
    @review = @shipper.create_review(review_params.merge(user_id: current_user.id))
    if @review.save
      @shipper.user.notifications.create(send_id: current_user.id, content: "Bạn vừa được đánh giá #{@review.score} sao từ #{current_user.profile.name}")
      render :json => {:success => true, :message => "Đánh giá thành công"}
    else
      render :json => {:success => false, :message => "Đánh giá thất bại"}
    end
    # redirect_to my_post_true_path
  end

  private

  def review_params
    params.require(:review).permit(:score, :comment, :shipper_id)
  end
end
