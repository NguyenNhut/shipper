class ConfigurableController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user
  before_action :check_locked
  before_action :authenticate_profile

  def edit
    @configurable = Configurable.first
  end

  def update
    @configurable = Configurable.first
    if @configurable.update configurable_params
      flash[:success] = "Chỉnh sửa thành công"
      redirect_to edit_configurable_path(@configurable.id)
    else
      flash[:errors] = "Chỉnh sửa  thất bại"
      render :edit
    end
  end

  private

  def configurable_params
    params.require(:configurable).permit(:price_near, :price_away, :percent_fee)
  end

end
