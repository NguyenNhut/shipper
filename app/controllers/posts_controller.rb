class PostsController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user
  before_action :check_locked
  before_action :authenticate_profile
  impressionist :actions=>[:show]
  before_action :set_post, only: [:edit, :update, :destroy, :show]
  def index
    # @posts = Post.where(status: false).order({ created_at: :desc })
    if params[:search]
      @posts = Post.where(status: false).search(params[:search][:place_from], params[:search][:place_to]).order({ created_at: :desc }).paginate(:page => params[:page], :per_page => 10)
    else
      @posts = Post.where(status: false).order({ created_at: :desc }).paginate(:page => params[:page], :per_page => 10)
    end
  end

  def new
    @post = Post.new
  end

  def calculate_price
    @configurable = Configurable.first
    distance = (params[:distance].to_f/1000+0.5).to_i

    if distance <= 3
      price = @configurable.price_near
    else
      price = @configurable.price_near + @configurable.price_away*(distance - 3)
    end
    render :json => {:success => true, :data => {:price => price, :distance => distance}}
  end

  def create
    @post = current_user.posts.new post_params
    if @post.save
      flash[:success] = "Thêm mới thành công"
      redirect_to my_post_false_path
    else
      flash[:errors] = "Thêm mới thất bại"
      render :new
    end
  end

  def show
    impressionist(@post)
  end

  def edit
  end

  def update
    if @post.update post_params
      flash[:success] = "Cập nhật thành công"
      redirect_to my_post_false_path
    else
      flash[:errors] = "Cập nhật thất bại"
      render :edit
    end
  end

  def destroy
    if @post.destroy
      counter_post = current_user.posts.count
      render :json => {:success => true, :message => "Xóa thành công", :counter_post => counter_post}
    else
      render :json => {:success => false, :message => "Xóa thất bại"}
    end
  end

  def my_post_false
    @posts = current_user.posts.where(status: false).order({ created_at: :desc })
  end

  def my_post_true
    @posts = current_user.posts.where(status: true).order({ created_at: :desc })
  end

  def my_post_details
    @post = Post.find_by id: params[:post_id]
  end


  private

  def set_post
    @post = Post.find_by id: params[:id]
  end

  def post_params
    params.require(:post).permit(:user_id, :title, :content, :place_from, :place_to, :time_receive, :time_send, :price, :status)
  end

end
