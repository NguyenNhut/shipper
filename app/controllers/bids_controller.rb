class BidsController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user
  before_action :check_locked
  before_action :authenticate_profile
  def create
    post= Post.find_by id: params[:bid][:post_id].to_i
    if post.user_id == current_user.id
      render :json => {:success => false, :message => "Đây là bài đăng của bạn"}
    else
      if current_user.profile.money < params[:bid][:bid_price].to_i* 0.1
        render :json => {:success => false, :message => "Tài khoản của bạn không đủ"}
      else
        @bid = post.bids.where(user_id: current_user.id)
        if params[:bid][:bid_price].to_i > 0
          if @bid.present? == false
            bid = current_user.bids.create bid_params
            if bid.save
              bid.post.user.notifications.create(send_id: current_user.id, content: "#{current_user.profile.name} vừa đấu giá vào 1 bài đăng của bạn")
              render :json => {:success => true, :message => "Bạn vừa đấu giá thành công"}
            else
              message = bid.errors.full_messages
              render :json => {:success => false, :message => message}
            end
          else
            render :json => {:success => false, :message => "Bạn chỉ được đấu giá 1 lần"}
          end
        else
          render :json => {:success => false, :message => "Giá không hợp lệ"}
        end
      end
    end
  end

  def my_bid
    @bids = current_user.bids.order({ created_at: :desc })
  end

  def destroy
    @bid = Bid.find_by_id params[:id]
    if @bid.destroy
      render :json => {:success => true, :message => "Xóa thành công"}
    else
      render :json => {:success => false, :message => "Xóa thất bại"}
    end
  end

  private

  def bid_params
    params.require(:bid).permit(:user_id, :post_id, :bid_price, :bid_comment)
  end
end
