class PaymentsController < ApplicationController
  # load_and_authorize_resource
  before_action :authenticate_user
  before_action :check_locked
  before_action :authenticate_profile

  def admin_payment
    @user = User.find_by id: params[:payment][:user_id].to_i
    if params[:payment][:money].to_i >= Configurable.first.money_payment
        @payment = Payment.new(payment_params.merge(card_token: "Admin"))
        if @payment.save
          money_payment = params[:payment][:money].to_i
          total_money = @user.profile.money + money_payment
          @user.profile.update money: total_money
          @user.notifications.create(send_id: current_user.id, content: "Tài khoản của bạn vừa được cộng thêm #{money_payment} vnđ")
          flash[:success] = "Nạp tiền thành công"
          message = "Nạp tiền thành công"
          render :json => {success: true, message: message}
        else
          message = "Nạp tiền thất bại"
          render :json => {success: false, message: message}
        end
      else
        message = "Số tiền phải từ #{Configurable.first.money_payment} trở lên"
        render :json => {success: false, message: message}
      end

  end

  # def new
  #   @ayment = Payment.new
  # end
    def new
    end

    def create
      @configurable = Configurable.first
      @customer = Stripe::Customer.create(
        email: params[:stripeEmail],
        source: params[:stripeToken]
      )

      charge = Stripe::Charge.create(
        customer: @customer.id,
        amount: @configurable.money_payment,
        description: 'Rails Stripe customer',
        currency: 'vnd'
      )

      if charge
        @user = current_user
        @payment = Payment.create(money: @configurable.money_payment, user_id: @user.id, card_token: @customer.sources.data[0]["id"])
        total_money = @user.profile.money + @configurable.money_payment
        @user.profile.update money: total_money
        @user.notifications.create(send_id: User.first.id, content: "Tài khoản của bạn vừa được cộng thêm #{@configurable.money_payment} vnđ")
        UserMailer.send_mail_payment(@user).deliver_now
        flash[:success] = "Nạp tiền thành công"
        redirect_to new_payment_path
      else
        flash[:error] = Stripe::CardError.message
        redirect_to new_payment_path
      end
    end

    def destroy
      @payment = Report.find_by id: params[:id]
      if @payment.destroy
        render :json => {:success => true, :message => "Xóa thành công"}
      else
        render :json => {:success => false, :message => "Xóa thất bại"}
      end
    end

    def payment_params
      params.require(:payment).permit(:money, :user_id)
    end
end
