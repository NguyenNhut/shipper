class UsersController < ApplicationController
     skip_before_action :verify_authenticity_token
     load_and_authorize_resource
     before_action :authenticate_user
     before_action :check_locked
     before_action :authenticate_profile
    def index
      @users = User.where(role: 0).search(params[:search]).order({ created_at: :desc }).paginate(:page => params[:page], :per_page => 20)
    end

    def show
      @user = User.find_by id: params[:id]
    end

    def destroy
      @user = User.find_by id: params[:id]
      if @user.destroy
        render :json => {:success => true}
      else
        render :json => {:success => false}
      end
    end

    def details
      @user = User.find_by id: params[:user_id]
    end

    def change_password
      @user = User.find_by id: current_user.id
      if @user.valid_password? params[:old_password]
        if params[:new_password] == params[:renew_password]
          if @user.update password: params[:new_password]
            flash[:success] = "Thay đổi thành công"
            sign_in @user, bypass: true
            redirect_to profile_path(@user.profile.id)
          else
            flash[:errors] = "Thay đổi thất bại"
            redirect_to edit_profile_path(@user.profile.id)
          end
        else
          flash[:errors] = "Mật khẩu không khớp"
          redirect_to edit_profile_path(@user.profile.id)
        end
      else
        flash[:errors] = "Mật khẩu cũ không đúng"
        redirect_to edit_profile_path(@user.profile.id)
      end
    end

    def lock_user
      @user = User.find_by id: params[:user_id]
      if @user.lock
        @user.update(lock: false, date_lock: nil)
        message = "Đã mở khóa người dùng"
        UserMailer.send_mail_unlock(@user).deliver_now
      else
        @user.update(lock: true, date_lock: Time.now)
        message = "Đã khóa người dùng"
        UserMailer.send_mail_lock(@user).deliver_now
      end
      locked = @user.lock
      render :json => {:success => true, :message => message, :locked => locked }
    end

end
