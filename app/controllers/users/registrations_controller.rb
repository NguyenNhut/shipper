class Users::RegistrationsController < Devise::RegistrationsController
  respond_to :json
  # skip_before_action :authenticate_user
  # skip_before_action :authenticate_profile

  def create
    if params[:user][:password] == params[:user][:password_comfirmation]
        build_resource(sign_up_params)
        resource.save
        yield resource if block_given?
        if resource.persisted?
          # sign_up(resource_name, resource)
          return render :json => {:success => true,:message => 'Vui lòng xác thực email để quá trình đăng ký được hoàn tất.', :data => resource}
        else
          clean_up_passwords resource
          messages = resource.errors.messages
          return render :json => {:success => false, :message => 'Email đã tồn tại'}
        end
      else
        clean_up_passwords resource
        messages = "Mật khẩu không khớp"
        return render :json => {:success => false, :message => messages}
    end
  end

  # Signs in a user on sign up. You can overwrite this method in your own
  # RegistrationsController.
  def sign_up(resource_name, resource)
    sign_in(resource_name, resource)
  end
end
