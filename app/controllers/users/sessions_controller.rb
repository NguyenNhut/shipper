class Users::SessionsController < Devise::SessionsController
  respond_to :json

  def create
    resource = User.find_by_email params[:user][:email]
    if resource && resource.valid_password?(params[:user][:password])
      if resource.confirmed?
        sign_in(resource_name, resource)
        message = "Đăng nhập thành công"
        flash[:success] = "Đăng nhập thành công"

        # if resource.has_profile
          if resource.is_admin
            render :json => {:success => true, :login => true, :message => message, :confirmed => true, :admin => true }, status: 200
          else
            render :json => {:success => true, :login => true, :message => message, :confirmed => true, :admin => false }, status: 200
          end
        # else
        #   render :json => {:success => true, :login => true, :message => message, :confirmed => true }, status: 200
        # end
      else
        message = "Vui lòng xác thực email"
        render :json => {:success => true, :login => false, :message => message, :confirmed => false }
      end
    else
      message = "Email hoặc mật khẩu không đúng"
      return render :json => {:success => false, :login => false, :message => message}
    end
  end

 def signout
   sign_out(current_user)
 end
end
