class ProfilesController < ApplicationController
  load_and_authorize_resource
  before_action :authenticate_user!
  before_action :check_locked

  # skip_before_action :authenticate_profile, only: [:new, :create]
  Paperclip::Attachment.default_options[:default_url] = "/assets/user.png"
  def new
    @profile = Profile.new
  end

  def create
    @profile = current_user.create_profile profile_params
    if @profile.save
      flash[:success] = "Cập nhật thành công"
      redirect_to profile_path(@profile.id)
    else
      flash[:error] = "Cập nhật thất bại"
      render :new
    end
  end

  def show
    @profile = Profile.find_by id: params[:id]
  end

  def edit
    @profile = Profile.find_by id: params[:id]
  end

  def update
    @profile = Profile.find_by id: params[:id]
    if @profile.update profile_params
      flash[:success] = "Cập nhật thành công"
      redirect_to profile_path(@profile.id)
    else
      flash[:error] = "Cập nhật thất bại"
      render :edit
    end
  end

  private
  def profile_params
    profile_params = params.require(:profile).permit(:name, :date_of_birth, :male, :address, :phone, :identity_card, :avatar, :user_id)
  end
end
