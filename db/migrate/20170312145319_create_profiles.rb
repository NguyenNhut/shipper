class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.belongs_to :user, index: true
      t.string :name
      t.date :date_of_birth
      t.string :male
      t.string :address
      t.string :phone
      t.integer :identity_card
      t.integer :money, default: 0
      t.attachment :avatar
      t.timestamps
    end
  end
end
