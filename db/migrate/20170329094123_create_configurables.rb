class CreateConfigurables < ActiveRecord::Migration[5.0]
  def change
    create_table :configurables do |t|
      t.integer :price_near
      t.integer :price_away
      t.integer :percent_fee
      t.integer :money_payment

      t.timestamps
    end
  end
end
