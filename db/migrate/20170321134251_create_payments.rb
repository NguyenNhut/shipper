class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.belongs_to :user
      t.integer :money

      t.timestamps
    end
  end
end
