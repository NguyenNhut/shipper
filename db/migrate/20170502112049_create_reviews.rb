class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.belongs_to :user
      t.belongs_to :shipper
      t.integer :score
      t.string :comment

      t.timestamps
    end
  end
end
