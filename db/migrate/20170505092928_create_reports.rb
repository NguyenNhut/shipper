class CreateReports < ActiveRecord::Migration[5.0]
  def change
    create_table :reports do |t|
      t.belongs_to :user
      t.integer :user_report
      t.string :comment

      t.timestamps
    end
  end
end
