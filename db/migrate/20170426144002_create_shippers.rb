class CreateShippers < ActiveRecord::Migration[5.0]
  def change
    create_table :shippers do |t|
      t.belongs_to :user
      t.belongs_to :post
      t.integer :money_ship
      t.integer :fee

      t.timestamps
    end
  end
end
