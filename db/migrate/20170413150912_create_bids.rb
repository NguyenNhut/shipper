class CreateBids < ActiveRecord::Migration[5.0]
  def change
    create_table :bids do |t|
      t.belongs_to :user
      t.belongs_to :post
      t.integer :bid_price
      t.string :bid_comment
      t.timestamps
    end
  end
end
