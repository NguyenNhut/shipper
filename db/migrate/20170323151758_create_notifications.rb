class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.integer :send_id
      t.belongs_to :user
      t.string :content
      t.boolean :status, default: false

      t.timestamps
    end
  end
end
