class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.belongs_to :user
      t.string :title
      t.string :content
      t.string :place_from
      t.string :place_to
      t.datetime :time_receive
      t.datetime :time_send
      t.integer :price
      t.boolean :status, default: false

      t.timestamps
    end
  end
end
