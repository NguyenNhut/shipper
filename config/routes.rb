Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  devise_for :users, :controllers => {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    confirmations: 'users/confirmations'
  }
    resources :profiles
    resources :users, only: [:index, :destroy, :show] do
      get 'details'
      get 'lock_user'
    end
    resources :payments
    post 'admin_payment' => 'payments#admin_payment'

    resources :notifications, only: [:index, :destroy]
    resources :configurable, only: [:edit, :update]
    post 'seen_notifications' => 'notifications#seen_notifications'
    post 'change_password' => 'users#change_password'
    post 'calculate_price' => 'posts#calculate_price'
    resources :bids
    resources :posts do
      get 'my_post_details'
    end
    get 'my_post_false' => 'posts#my_post_false'
    get 'my_post_true' => 'posts#my_post_true'
    get 'my_bid' => 'bids#my_bid'

    resources :shippers
    resources :reviews, only: [:index, :create]
    resources :reports, only: [:create, :destroy]
    resources :warning, only: :index


 root :to => 'home#index'
end
