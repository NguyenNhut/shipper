env :PATH, ENV['PATH']
env :GEM_PATH, ENV['GEM_PATH']
# $ RAILS_ENV=production my_script


set :environment, "development"
set :output, {:error => 'error.log', :standard => 'standard.log'}
every :day, :at => '7:00 am'  do
# every 1.minutes do
  runner "User::unlock_user"
end

every 1.minutes do
  runner "Post::delete_post_deadline"
end
